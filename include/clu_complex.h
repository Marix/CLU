/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#ifdef __cplusplus
extern "C" {
#endif
	
/**
 * This is an implementation of a compelex data type for on device computation.
 *
 * It _should_ be compatible with stdc++ complex<T>
 */
typedef struct {
	float real;
	float imag;
} cluComplex32;

inline cluComplex32 make_cluComplex32( float real, float imag )
{
	return (cluComplex32) { real, imag };
}
	
inline cluComplex32 add_cluComplex32( cluComplex32 left, cluComplex32 right )
{
	return make_cluComplex32(
		left.real + right.real,
		left.imag + right.imag
	);
}

inline cluComplex32 sub_cluComplex32( cluComplex32 left, cluComplex32 right )
{
	return make_cluComplex32(
		left.real - right.real,
		left.imag - right.imag
	);
}

inline cluComplex32 mul_cluComplex32( cluComplex32 left, cluComplex32 right )
{
	return make_cluComplex32(
		left.real * right.real - left.imag * right.imag,
		left.real * right.imag + left.imag * right.real
	);
}

inline cluComplex32 div_cluComplex32_float( cluComplex32 left, float right )
{
	return make_cluComplex32(
		left.real / right,
		left.imag / right
	);
}

inline cluComplex32 div_cluComplex32( cluComplex32 left, cluComplex32 right )
{
	float denom = right.real * right.real + right.imag * right.imag;

	return make_cluComplex32(
		( left.real * right.real + left.imag * right.imag ) / denom,
		( left.imag * right.real - left.real * right.imag ) / denom
	);
}
	
	
#ifdef __cplusplus
} // closing brace for extern "C"
#endif
