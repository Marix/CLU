/*
 * This file is part of CLU.
 *
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

/**
 * GPU type for taus state
 */
typedef uint4 clu_taus_state;

/**
 * Generates the next random number using a hybrid Tausworthe PRNG.
 *
 * This is based on an article in GPU Gems 3, see
 * http://http.developer.nvidia.com/GPUGems3/gpugems3_ch37.html
 * for details.
 *
 * The generated random numbers are evenly distributed. The state is modified.
 * Before the first call the state should be initializes using some random RNG.
 * The initial values for the first the x, y and z components of the state should
 * be > 128.
 *
 * The state is assumed to have been properly initialized, e.g. by calling
 * void cluTausSeedStates( clu_taus_state* state, size_t elems ) on the CPU
 * and copying the result to the memory object.
 */
inline float clu_taus_rnd( __global clu_taus_state* state );

/* FORWARD DECLARATION OF INTERNAL FUNCTIONS */
inline unsigned _clu_taus_step( unsigned * z, int S1, int S2, int S3, unsigned M);
inline unsigned _clu_LCG_step( unsigned * z, unsigned A, unsigned C);

///* IMPLEMENTATIONS */

inline float clu_taus_rnd( __global clu_taus_state* state )
{
	uint x,y,z,w;
	x = (*state).x;
	y = (*state).y;
	z = (*state).z;
	w = (*state).w;
	float res = 2.328306436538696e-10f*( _clu_taus_step( &x, 13, 19, 12, 4294967294ul)^
	                                     _clu_taus_step( &y, 2, 25, 4,   4294967288ul)^
	                                     _clu_taus_step( &z, 3, 11, 17,  4294967280ul)^
	                                     _clu_LCG_step(  &w, 1664525,    1013904223ul) );
	*state = (clu_taus_state)(x,y,z,w);
	return res;
}

inline unsigned _clu_taus_step( unsigned *z, int S1, int S2, int S3, unsigned M)
{
	unsigned b = ( ( ( (*z) << S1 )^(*z) ) >> S2 );
	return *z = ( ( ( (*z) & M ) << S3 )^b );
}

inline unsigned _clu_LCG_step( unsigned *z, unsigned A, unsigned C)
{
	return *z= ( A * (*z) + C );
}

