/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

/**
 * Return a human readable representation of the OpenCL error code.
 */
const char* cluGetErrorString( const cl_int errcode );

/**
 * A utility function that can be used as a callback in clBuildProgram
 */
void cluContextNotifyStdout( const char * errinfo, const void * private_info, size_t ch, void * usr_data );
void cluContextNotifyStderr( const char * errinfo, const void * private_info, size_t ch, void * usr_data );
void cluContextNotifyStderrAndAbort( const char * errinfo, const void * private_info, size_t ch, void * usr_data );

/**
 * Reads a program from a source file
 */
cl_program cluCreateProgramFromSource( cl_context context, const char* filename, const char* sourceDir, cl_int *errcode_ret );

/*
 * DEVICE INFORMATION
 */

/**
 * Check whether a given OpenCL extension is available for the given device
 */
cl_int cluCheckForExtension( cl_device_id device, const char* extension_name, cl_bool * available );

/**
 * Get the peak performanace of the device in Gflops.
 *
 * \param The id of the device to be queried
 * \param If set queries the double precision performance, otherwise single precision performance is returned
 * \param Memory address to which the result will be written, 0.0f if unkown.
 */
cl_int cluGetPeakPerformance( cl_device_id device, cl_bool doublePrecision, cl_float * performance );

/**
 * Get the peak memory bandwidth of the device in Gbytes
 *
 * \param The id of the device to be queried
 * \param If set queries the double precision performance, otherwise single precision performance is returned
 * \param Memory address to which the result will be written, 0.0f if unkown.
 */
cl_int cluGetPeakBandwidth( cl_device_id device, cl_float * bandwidth );

/*
 * RANDOM NUMBER GENERATION
 */

/**
 * The CPU definition of the state of a taus
 */
typedef cl_uint4 clu_taus_state;

/**
 * A utility function to seed the taus state using the standard rand48()-Function.
 *
 * Don't forget to srand48() before if you want reproducable resutls.
 *
 * \param[out] The taus state to initialize
 *
 * \deprecated Initialization using a week random number generator is probably not the best idea.
 */
void cluTausSeedState( clu_taus_state* state );

/**
 * A utility function to seed the taus state for a whole NDRange of generators from values in a file.
 *
 * The file is expected to contain random bytes, which will be interpreted as unsigned integers.
 * Such a file can be retrieved from http://random.org/bytes for example.
 *
 * \param[out] The taus states to initialize
 * \param[in] The number of states to initialize
 * \param[in] Name (location) of a file to read one integer per line from. Some values might be skipped,
 *            so the file needs to be sufficiently larger then elems.
 * \return    CLU_INVALID_FILE if the file cannot be found
 *            CL_INVALID_VALUE if the file does not contain enough bytes
 */
cl_int cluTausSeedStatesFromFile( clu_taus_state* state, size_t elems, char* file );

/*
 * Constants for error values
 */
#define CLU_INVALID_FILE -257

/*
 * Macros to work around vendor errors
 */

/**
 * Allow access to opencl vector types on the host in a compatible fashion.
 */
#if defined(__APPLE__) && !defined(CL_VERSION_1_1)
#define CLU_VEC( vec, idx ) (vec)[idx]
#else /* __APPLE__ */
#define CLU_VEC( vec, idx ) (vec).s[idx]
#endif /* __APPLE__ */

// Yes, I know this doesn't produce a beautiful syntax

#ifdef __cplusplus
} // closing brace for extern "C"
#endif
