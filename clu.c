/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include "clu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

const char* cluGetErrorString( const cl_int errcode )
{
	switch ( errcode ) {
		case CL_SUCCESS:
			return "Success";
		case CL_DEVICE_NOT_FOUND:
			return "Device not found";
		case CL_DEVICE_NOT_AVAILABLE:
			return "Device not available";
		case CL_COMPILER_NOT_AVAILABLE:
			return "Compiler not available";
		case CL_MEM_OBJECT_ALLOCATION_FAILURE:
			return "Memory object allocation failure";
		case CL_OUT_OF_RESOURCES:
			return "Out of resources";
		case CL_OUT_OF_HOST_MEMORY:
			return "Out of host memory";
		case CL_PROFILING_INFO_NOT_AVAILABLE:
			return "Profiling information not available";
		case CL_MEM_COPY_OVERLAP:
			return "Memory copy overlap";
		case CL_IMAGE_FORMAT_MISMATCH:
			return "Image format mismatch";
		case CL_IMAGE_FORMAT_NOT_SUPPORTED:
			return "Image format not supported";
		case CL_BUILD_PROGRAM_FAILURE:
			return "Build program failure";
		case CL_MAP_FAILURE:
			return "Map failure";
		
		case CL_INVALID_VALUE:
			return "Invalid value";
		case CL_INVALID_DEVICE_TYPE:
			return "Invalid device type";
		case CL_INVALID_PLATFORM:
			return "Invalid platform";
		case CL_INVALID_DEVICE:
			return "Invalid device";
		case CL_INVALID_CONTEXT:
			return "Invalid context";
		case CL_INVALID_QUEUE_PROPERTIES:
			return "Invalid queue properties";
		case CL_INVALID_COMMAND_QUEUE:
			return "Invalid command queue";
		case CL_INVALID_HOST_PTR:
			return "Invalid host pointer";
		case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
			return "Invalid image format descriptor";
		case CL_INVALID_IMAGE_SIZE:
			return "Invalid image size";
		case CL_INVALID_SAMPLER:
			return "Invalid sampler";
		case CL_INVALID_BINARY:
			return "Invalid binary";
		case CL_INVALID_BUILD_OPTIONS:
			return "Invalid build options";
		case CL_INVALID_PROGRAM:
			return "Invalid program";
		case CL_INVALID_PROGRAM_EXECUTABLE:
			return "Invalid program executable";
		case CL_INVALID_KERNEL_NAME:
			return "Invalid kernel name";
		case CL_INVALID_KERNEL_DEFINITION:
			return "Invalid kernel definition";
		case CL_INVALID_KERNEL:
			return "Invalid kernel";
		case CL_INVALID_ARG_INDEX:
			return "Invalid argument index";
		case CL_INVALID_ARG_SIZE:
			return "Invalid argument size";
		case CL_INVALID_KERNEL_ARGS:
			return "Invalid kernel arguments";
		case CL_INVALID_WORK_DIMENSION:
			return "Invalid work dimension";
		case CL_INVALID_WORK_GROUP_SIZE:
			return "Invalid work group size";
		case CL_INVALID_WORK_ITEM_SIZE:
			return "Invalid work item size";
		case CL_INVALID_GLOBAL_OFFSET:
			return "Invalid global offset";
		case CL_INVALID_EVENT_WAIT_LIST:
			return "Invalid event wait list";
		case CL_INVALID_EVENT:
			return "Invalid event";
		case CL_INVALID_OPERATION:
			return "Invalid operation";
		case CL_INVALID_GL_OBJECT:
			return "Invalid OpenGL object";
		case CL_INVALID_BUFFER_SIZE:
			return "Invalid buffer size";
		case CL_INVALID_MIP_LEVEL:
			return "Invalid miplevel";
			
		case CLU_INVALID_FILE:
			return "Invalid file";

		default:
			return "Unkown errorcode";
	}
}

void _cluNotify( FILE* target, const char * errinfo, __attribute__((unused)) const void * private_info, __attribute__((unused)) size_t ch, void * usr_data )
{
	if( usr_data )
	{
		fprintf( target, "%s - %s\n", (char*) usr_data, errinfo );
	}
	else
	{
		fprintf( target, "%s\n", errinfo );
	}
}
void cluContextNotifyStdout( const char * errinfo, const void * private_info, size_t ch, void * usr_data )
{
	_cluNotify( stdout, errinfo, private_info, ch, usr_data );
}
void cluContextNotifyStderr( const char * errinfo, const void * private_info, size_t ch, void * usr_data )
{
	_cluNotify( stderr, errinfo, private_info, ch, usr_data );
}
void cluContextNotifyStderrAndAbort( const char * errinfo, const void * private_info, size_t ch, void * usr_data )
{
	_cluNotify( stderr, errinfo, private_info, ch, usr_data );
	exit( -13 );
}

cl_program cluCreateProgramFromSource( cl_context context, const char* filename, const char* sourceDir, cl_int *errcode_ret )
{
	FILE* file;
	char* buffer;
	cl_program program = 0;
	size_t fileSize;
	char* fullname;
	size_t filenameLen, fullnameLen;
	
	// open file
	// first create a proper filename
	// TODO fix for redmond os
	filenameLen = strlen( filename );
	if( sourceDir )
	{
		if( filename[ filenameLen - 1 ] != '/' )
		{
			fullnameLen = strlen( sourceDir ) + 1 + filenameLen + 1;
			fullname = (char*) malloc( fullnameLen );
			if( ! fullname )
			{
				if( errcode_ret )
					*errcode_ret = CL_OUT_OF_HOST_MEMORY;
				return program;
			}
			sprintf( fullname, "%s/%s", sourceDir, filename);
		}
		else
		{
			fullnameLen = strlen( sourceDir ) + filenameLen + 1;
			fullname = (char*) malloc( fullnameLen );
			if( ! fullname )
			{
				if( errcode_ret )
					*errcode_ret = CL_OUT_OF_HOST_MEMORY;
				return program;
			}
			sprintf( fullname, "%s%s", sourceDir, filename);
		}
	}
	else
	{
		fullnameLen = filenameLen;
		fullname = (char*) malloc( fullnameLen + 1 );
		strcpy( fullname, filename );
	}
	// finally open that thing, as you may have noted, the fullname might be relative
	file = fopen( fullname, "rb" );
	free( fullname );
	if( ! file )
	{
		if( errcode_ret )
		{
			if( errno == ENOMEM )
				*errcode_ret = CL_OUT_OF_HOST_MEMORY;
			else
				*errcode_ret = CLU_INVALID_FILE;
		}
		return program;
	}
	
	// get filesize
	fseek( file, 0L, SEEK_END );
	fileSize = ftell( file );
	fseek( file, 0L, SEEK_SET );
	
	// allocate buffer
	buffer = (char*) malloc( fileSize );
	if( ! buffer )
	{
		fclose( file );
		if( errcode_ret )
			*errcode_ret = CL_OUT_OF_HOST_MEMORY;
		return program;
	}
	
	// read file
	if( fread( buffer, fileSize, 1, file ) != 1 )
	{
		// we tried to read the whole file once and failed
		free( buffer );
		fclose( file );
		if( errcode_ret )
			*errcode_ret = CLU_INVALID_FILE;
		return program;
	}
			
	
	// have OpenCL create the file
	{
		const char* ro_buffer = (const char*) buffer;
		program = clCreateProgramWithSource( context, 1, &ro_buffer, &fileSize, errcode_ret );
	}
	
	// free resources
	free( buffer );
	fclose( file );
	
	return program;
}

cl_int cluCheckForExtension( cl_device_id device, const char* extension_name, cl_bool * available )
{
	// retrieve all extensions from OpenCL
	const size_t MAX_EXTENSION_CANDIDATE_STRING_SIZE = 512;
	size_t candidatesSize;
	char* candidates = (char*) malloc( MAX_EXTENSION_CANDIDATE_STRING_SIZE );
	if( candidates == NULL )
		return CL_OUT_OF_HOST_MEMORY;
	
	cl_int err = clGetDeviceInfo( device, CL_DEVICE_EXTENSIONS, MAX_EXTENSION_CANDIDATE_STRING_SIZE, candidates, &candidatesSize );
	if( err )
	{
		free( candidates );
		return err;
	}
	
	// scan for the requested extension (somewhat overly complicated formulated, but it shows the intention better and the compiler should take care of it. shouldn't be used on critical path anyway)
	*available = ( strstr( candidates, extension_name ) == NULL ? 0 : 1 ); 
	
	return CL_SUCCESS;
}

cl_int cluGetPeakPerformance( cl_device_id device, cl_bool doublePrecision, cl_float * performance )
{
	char * name;
	size_t nName;
	cl_int err = CL_SUCCESS;

	*performance = 0.0f;

	// get device name
	err = clGetDeviceInfo( device, CL_DEVICE_NAME, 0, NULL, &nName );
	if( err )
		return err;
	name = (char*) malloc( nName );
	if( name == NULL )
		return CL_OUT_OF_HOST_MEMORY;
	err = clGetDeviceInfo( device, CL_DEVICE_NAME, nName, name, &nName );
	if( err )
		return err;

	// TODO handle CPUs

	if( doublePrecision )
		return CL_SUCCESS;

	if( strstr( name, "NVIDIA" ) != NULL )
	{
		// handle know NVIDIA GPUS
		// TODO implement
	}
	else
	{
		// assume AMD GPU

		// use number of compute units to distinguish gpus with same codename
		cl_uint computeUnits;
		err = clGetDeviceInfo( device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(computeUnits), &computeUnits, NULL );
		if( err )
			return err;

		// handle known AMD GPUS
		else if( strstr( name, "Cedar" ) != NULL )
		{
			*performance = 104.f;
		}
		else if( strstr( name, "Redwood" ) != NULL )
		{
			if( computeUnits == 4 )
				*performance = 352.f;
			else if( computeUnits == 5 )
				*performance = 620.f;
				// FIXME broken on Redwood PRO
		}
		else if( strstr( name, "Juniper" ) != NULL )
		{
			if( computeUnits == 9 )
				*performance = 1008.f;
			else if( computeUnits == 10 )
				*performance = 1360.f;
		}
		else if( strstr( name, "Cypress" ) != NULL )
		{
			if( computeUnits == 14 )
				*performance = 1792.f;
			else if( computeUnits == 18 )
				*performance = 2088.f;
			else if( computeUnits == 20 )
				*performance = 2720.f;
			// FIXME wrong result on hamloc
		}
		// TODO implement
	}

	// for anything else we don't have specs, we initialized to 0, so do nothing

	free( name );
	return CL_SUCCESS;
}

/**
 * Get the peak memory bandwidth of the device in Gbytes
 *
 * \param The id of the device to be queried
 * \param If set queries the double precision performance, otherwise single precision performance is returned
 * \param Memory address to which the result will be written, 0.0f if unkown.
 */
cl_int cluGetPeakBandwidth( cl_device_id device, cl_float * bandwidth )
{
	char * name;
	size_t nName;
	cl_int err;

	*bandwidth = 0.0f;

	// get device name
	err = clGetDeviceInfo( device, CL_DEVICE_NAME, 0, NULL, &nName );
	if( err )
		return err;
	name = (char*) malloc( nName );
	if( name == NULL )
		return CL_OUT_OF_HOST_MEMORY;
	err = clGetDeviceInfo( device, CL_DEVICE_NAME, nName, name, &nName );
	if( err )
		return err;

	// TODO handle CPUs

	if( strstr( name, "NVIDIA" ) != NULL )
	{
		// handle know NVIDIA GPUS
		/*
		 * Generation 8
		 */
		if( strstr( name, "8800 Ultra" ) != NULL )
		{
			*bandwidth = 103.7f;
		}
		else if( strstr( name, "8800 GTX" ) != NULL )
		{
			*bandwidth = 86.4f;
		}
		else if( strstr( name, "8800 GTS" ) != NULL )
		{
			*bandwidth = 64.f;
		}
		else if( strstr( name, "8800 GS" ) != NULL )
		{
			*bandwidth = 38.4f;
		}
		else if( strstr( name, "8600 GTS" ) != NULL )
		{
			*bandwidth = 32.f;
		}
		else if( strstr( name, "8600 GT" ) != NULL )
		{
			*bandwidth = 22.4f;
		}
		else if( strstr( name, "8500 GT" ) != NULL )
		{
			*bandwidth = 12.8f;
		}
		else if( strstr( name, "8400 GS" ) != NULL )
		{
			*bandwidth = 6.4f;
		}
		/*
		 * Generation 9
		 */
		else if( strstr( name, "9800 GX2" ) != NULL )
		{
			*bandwidth = 128 / 2;
		}
		else if( strstr( name, "9800 GTX" ) != NULL )
		{
			*bandwidth = 70.4f;
		}
		else if( strstr( name, "9800 GT" ) != NULL )
		{
			*bandwidth = 57.6f;
		}
		else if( strstr( name, "9600 GT" ) != NULL )
		{
			*bandwidth = 57.6f;
		}
		else if( strstr( name, "9600 GSO 512" ) != NULL )
		{
			*bandwidth = 57.6f;
		}
		else if( strstr( name, "9600 GSO" ) != NULL )
		{
			*bandwidth = 38.4f;
		}
		else if( strstr( name, "9500 GT" ) != NULL )
		{
			*bandwidth = 25.6f;
		}
		else if( strstr( name, "9400 GT" ) != NULL )
		{
			*bandwidth = 12.4f;
		}
		/*
		 * 100 Series
		 */
		else if( strstr( name, "GT 120" ) != NULL )
		{
			*bandwidth = 16.0f;
		}
		else if( strstr( name, "G100" ) != NULL )
		{
			*bandwidth = 8.0f;
		}
		/*
		 * 200 Series
		 */
		else if( strstr( name, "GTX 295" ) != NULL )
		{
			*bandwidth = 223.8f / 2;
		}
		else if( strstr( name, "GTX 285" ) != NULL )
		{
			*bandwidth = 159.0f;
		}
		else if( strstr( name, "GTX 280" ) != NULL )
		{
			*bandwidth = 141.7f;
		}
		else if( strstr( name, "GTX 275" ) != NULL )
		{
			*bandwidth = 127.0f;
		}
		else if( strstr( name, "GTX 260" ) != NULL )
		{
			*bandwidth = 111.9f;
		}
		else if( strstr( name, "GTS 250" ) != NULL )
		{
			*bandwidth = 70.4f;
		}
		else if( strstr( name, "GTS 240" ) != NULL )
		{
			*bandwidth = 70.4f;
		}
		else if( strstr( name, "GT 240" ) != NULL )
		{
			*bandwidth = 57.6f;
		}
		else if( strstr( name, "GT 220" ) != NULL )
		{
			*bandwidth = 25.3f;
		}
		else if( strstr( name, "210" ) != NULL )
		{
			*bandwidth = 8.0f;
		}
		else if( strstr( name, "205" ) != NULL )
		{
			*bandwidth = 8.0f;
		}
		/*
		 * 200 Series
		 */
		else if( strstr( name, "GTX 480" ) != NULL )
		{
			*bandwidth = 177.4f;
		}
		else if( strstr( name, "GTX 470" ) != NULL )
		{
			*bandwidth = 133.9f;
		}
		else if( strstr( name, "GTX 465" ) != NULL )
		{
			*bandwidth = 102.6f;
		}
		else if( strstr( name, "GTX 460" ) != NULL )
		{
			*bandwidth = 108.8f;
		}
		else if( strstr( name, "GTS 450" ) != NULL )
		{
			*bandwidth = 57.7f;
		}
		else if( strstr( name, "GT 440" ) != NULL )
		{
			*bandwidth = 51.2f;
		}
		else if( strstr( name, "GT 430" ) != NULL )
		{
			*bandwidth = 28.8f;
		}
		/*
		 * 300 Series
		 */
		else if( strstr( name, "GT 340" ) != NULL )
		{
			*bandwidth = 54.4f;
		}
		else if( strstr( name, "GT 330" ) != NULL )
		{
			*bandwidth = 32.f;
		}
		else if( strstr( name, "GT 320" ) != NULL )
		{
			*bandwidth = 25.3f;
		}
		else if( strstr( name, "310" ) != NULL )
		{
			*bandwidth = 8.0f;
		}
		else if( strstr( name, "315" ) != NULL )
		{
			*bandwidth = 12.6f;
		}
		/*
		 * 400 Series
		 */
		else if( strstr( name, "GTX 460" ) != NULL )
		{
			*bandwidth = 108.8f;
		}
		else if( strstr( name, "GTS 450" ) != NULL )
		{
			*bandwidth = 96.f;
		}
		else if( strstr( name, "GT 440" ) != NULL )
		{
			*bandwidth = 43.2f;
		}
		else if( strstr( name, "GT 430" ) != NULL )
		{
			*bandwidth = 28.8f;
		}
		else if( strstr( name, "GT 420" ) != NULL )
		{
			*bandwidth = 28.8f;
		}
		/*
		 * 500 Series
		 */
		else if( strstr( name, "GTX 590" ) != NULL )
		{
			*bandwidth = 327.7f / 2;
		}
		else if( strstr( name, "GTS 580" ) != NULL )
		{
			*bandwidth = 192.4f;
		}
		else if( strstr( name, "GTX 570" ) != NULL )
		{
			*bandwidth = 152.f;
		}
		else if( strstr( name, "GTX 560 Ti" ) != NULL )
		{
			*bandwidth = 128.f;
		}
		else if( strstr( name, "GTX 550 Ti" ) != NULL )
		{
			*bandwidth = 98.4f;
		}
	}
	else
	{
		// assume AMD

		// use number of compute units to distinguish gpus with same codename
		cl_uint computeUnits;
		err = clGetDeviceInfo( device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(computeUnits), &computeUnits, NULL );
		if( err )
			return err;

		// handle known AMD GPUS
		else if( strstr( name, "Cedar" ) != NULL )
		{
			*bandwidth = 13.f;
		}
		else if( strstr( name, "Redwood" ) != NULL )
		{
			if( computeUnits == 4 )
				*bandwidth = 26.f;
			else if( computeUnits == 5 )
				*bandwidth = 64.f;
				// FIXME broken on Redwood PRO
		}
		else if( strstr( name, "Juniper" ) != NULL )
		{
			if( computeUnits == 9 )
				*bandwidth = 74.f;
			else if( computeUnits == 10 )
				*bandwidth = 77.f;
		}
		else if( strstr( name, "Cypress" ) != NULL )
		{
			if( computeUnits == 14 )
				*bandwidth = 128.f;
			else if( computeUnits == 18 )
				*bandwidth = 128.f;
			else if( computeUnits == 20 )
				*bandwidth = 154.f;
			// FIXME wrong result on hamloc
		}
		// TODO implement
	}

	// for anything else we don't have specs, we initialized to 0, so do nothing

	free( name );
	return CL_SUCCESS;
}

