/*
 * This file is part of CLU.
 *
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include "clu.h"

#include <stdlib.h>
#include <stdio.h>

void cluTausSeedState( clu_taus_state* state )
{
	// why not directly assign to state?
	// 'cuz Apple sucks at implementing the standard it has the TM on
	cl_uint x, y, z, w;
	while( ( x = lrand48() ) <= 128 ) {};
	while( ( y = lrand48() ) <= 128 ) {};
	while( ( z = lrand48() ) <= 128 ) {};
	w = lrand48();
	// yes, that wouldn't need to be there, but just look at the
	// previous comment ...
	CLU_VEC(*state,0) = x;
	CLU_VEC(*state,1) = y;
	CLU_VEC(*state,2) = z;
	CLU_VEC(*state,3) = w;
}

void cluTausSeedStates( clu_taus_state* state, size_t elems )
{
	size_t i;
	for( i = 0; i < elems; ++i )
	{
		cluTausSeedState( &state[ i ] );
	}
}

cl_int cluTausSeedStatesFromFile( clu_taus_state *restrict state, size_t elems, char *restrict filename )
{
	FILE* file = fopen( filename, "rb" );

	if( ! file )
		return CLU_INVALID_FILE;

	size_t bytes_read = 0;
	for( size_t i_state = 0; i_state < elems; ++i_state )
	{
		int f_err = 1;

		unsigned int x = 0;
		while( x <= 128 && f_err == 1 )
		{
			f_err = fread( &x, sizeof(unsigned int), 1, file );
			bytes_read += sizeof(unsigned int);
		}

		unsigned int y = 0;
		while( y <= 128 && f_err == 1 )
		{
			f_err = fread( &y, sizeof(unsigned int), 1, file );
			bytes_read += sizeof(unsigned int);
		}

		unsigned int z = 0;
		while( z <= 128 && f_err == 1 )
		{
			f_err = fread( &z, sizeof(unsigned int), 1, file );
			bytes_read += sizeof(unsigned int);
		}

		unsigned int w;
		if( f_err == 1 )
		{
			f_err = fread( &w, sizeof(unsigned int), 1, file );
			bytes_read += sizeof(unsigned int);
		}


		if( f_err != 1 )
		{
			bytes_read -= sizeof(unsigned int); // we added after f_err was set to another value than 1
			fprintf( stderr, "Ran out of bytes after initializing %zu states using %zu bytes.\n", i_state, bytes_read );
			return CL_INVALID_VALUE;
		}

		CLU_VEC( state[i_state], 0 ) = x;
		CLU_VEC( state[i_state], 1 ) = y;
		CLU_VEC( state[i_state], 2 ) = z;
		CLU_VEC( state[i_state], 3 ) = w;
	}

	fclose( file );

	return CL_SUCCESS;
}
