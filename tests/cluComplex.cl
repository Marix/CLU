/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include "clu_complex.h"

__kernel void add32( unsigned int n, __write_only __global cluComplex32* out, __read_only __global cluComplex32* in_a, __read_only __global cluComplex32* in_b )
{
	// do nothing
	unsigned int i = get_global_id(0);
	if( i < n )
		out[ i ] = add_cluComplex32( in_a[i], in_b[i] );
}

__kernel void sub32( unsigned int n, __write_only __global cluComplex32* out, __read_only __global cluComplex32* in_a, __read_only __global cluComplex32* in_b )
{
	// do nothing
	unsigned int i = get_global_id(0);
	if( i < n )
		out[ i ] = sub_cluComplex32( in_a[i], in_b[i] );
}

__kernel void mul32( unsigned int n, __write_only __global cluComplex32* out, __read_only __global cluComplex32* in_a, __read_only __global cluComplex32* in_b )
{
	// do nothing
	unsigned int i = get_global_id(0);
	if( i < n )
		out[ i ] = mul_cluComplex32( in_a[i], in_b[i] );
}

__kernel void div32( unsigned int n, __write_only __global cluComplex32* out, __read_only __global cluComplex32* in_a, __read_only __global cluComplex32* in_b )
{
	// do nothing
	unsigned int i = get_global_id(0);
	if( i < n )
		out[ i ] = div_cluComplex32_float( in_a[i], in_b[i].real );
}

__kernel void div32_2( unsigned int n, __write_only __global cluComplex32* out, __read_only __global cluComplex32* in_a, __read_only __global cluComplex32* in_b )
{
	// do nothing
	unsigned int i = get_global_id(0);
	if( i < n )
		out[ i ] = div_cluComplex32( in_a[i], in_b[i] );
}

