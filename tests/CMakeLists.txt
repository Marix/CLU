message(STATUS "Building Tests")

set(Boost_USE_STATIC_LIBS   OFF)
find_package(Boost 1.34.0 REQUIRED COMPONENTS unit_test_framework)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS})

add_definitions( -DSOURCE_DIRECTORY="${CMAKE_SOURCE_DIR}/tests" -DADD_CL_INCLUDE_DIRS_COUNT=1 -DADD_CL_INCLUDE_DIRS={"${CMAKE_SOURCE_DIR}/include"})
add_executable(cluComplex cluComplex.cpp)
target_link_libraries(cluComplex clu ${Boost_LIBRARIES})
add_test(Complex cluComplex)

