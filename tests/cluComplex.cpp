// use the boost test framework
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE clu_complex
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

// Just for toying
#include <iostream>

// required for building the compile argument string
#include <sstream>

#include <exception>

// for reference
#include <complex>
#include <cstdlib>

#include <clu.h>

/**
 * Number of values to run each test on
 */
const cl_uint TEST_SIZE = 2048u;

/**
 * Somewhat ugly to define the context outside of the fixture, but
 * otherwise it is not available to the per-testcase fixture
 */
cl_context context;
cl_command_queue commandQueue;
cl_program program;
cl_device_id device;

/**
 * Fixture for OpenCL device managment
 */
struct DeviceManagment 
{
	DeviceManagment()
	{
		cl_int err;
		
		// initialize the device
		const cl_uint MAX_PLATFORMS = 4;
		cl_platform_id platforms[ MAX_PLATFORMS ];
		cl_uint nPlatforms = 0;
		
		cl_device_id devices[ 32 ];
		cl_uint nDevices;
		
		// try to get the platform (currently it should always be one)
		err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get platforms: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		if( ! nPlatforms )
		{
			BOOST_TEST_MESSAGE( "Failed to find an OpenCL platform." );
			throw std::exception();
		}
		
		if( nPlatforms > 1 )
		{
			BOOST_TEST_MESSAGE( "Found more than one OpenCL platform, will use a random one." );		
		}
		
		// currently there is always only one
		// As the AMD implementation already supports ICD we have to specify the platform
		// we want to use
		cl_context_properties context_props[3] = {
			CL_CONTEXT_PLATFORM,
			(cl_context_properties) platforms[0],
			0
		};
		
		context = clCreateContextFromType( context_props, CL_DEVICE_TYPE_ALL, cluContextNotifyStderr, NULL, &err );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to create context: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		// get devices from context
		{
			size_t returned_size;
			err = clGetContextInfo( context, CL_CONTEXT_DEVICES, sizeof( devices ), devices, &returned_size);
			if( err )
			{
				BOOST_TEST_MESSAGE( "Failed get Devices from Context: " << cluGetErrorString( err ) );
				throw std::exception();
			}
			nDevices = returned_size / sizeof( cl_device_id );
		}
		
		if( ! nDevices )
		{
			BOOST_TEST_MESSAGE( "No OpenCL devices found." );
			throw std::exception();
		}
		
		// just grab the first device
		device = devices[0];
		
		// loop over the devices to get their properties
		cl_device_type type;
		cl_uint vendor_id;
		cl_uint max_compute_units;
		size_t max_work_group_size;
		char name[ 64 ];
		size_t nName;
		cl_bool available;
		
		char log[ 2048 ];
		size_t logSize;
		
		const char* typeString;
		
		const char* compiler_options;
		
		err = clGetDeviceInfo( device, CL_DEVICE_TYPE, sizeof( type ), &type, NULL );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_TYPE: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		err = clGetDeviceInfo( device, CL_DEVICE_VENDOR_ID, sizeof( vendor_id ), &vendor_id, NULL );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_VENDOR_ID: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		err = clGetDeviceInfo( device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof( max_compute_units ), &max_compute_units, NULL );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_MAX_COMPUTE_UNITS: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		err = clGetDeviceInfo( device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof( max_work_group_size ), &max_work_group_size, NULL );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_MAX_WORK_GROUP_SIZE: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		err = clGetDeviceInfo( device, CL_DEVICE_NAME, sizeof( name ), name, &nName );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_NAME: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		err = clGetDeviceInfo( device, CL_DEVICE_AVAILABLE, sizeof( available ), &available, NULL );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get device property - CL_DEVICE_ENABLED: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		if( ! available )
		{
			BOOST_TEST_MESSAGE( "OpenCL device is not available" );
			throw std::exception();
		}

		switch( type )
		{
			case CL_DEVICE_TYPE_CPU:
				typeString = "CPU";
				break;
			case CL_DEVICE_TYPE_GPU:
				typeString = "GPU";
				break;
			case CL_DEVICE_TYPE_ACCELERATOR:
				typeString = "ACCELERATOR";
				break;
			case CL_DEVICE_TYPE_DEFAULT:
				typeString = "DEFAULT";
				break;
			default:
				typeString = "UNKNOWN";
				break;
		}
		
		BOOST_TEST_MESSAGE( "Running on " << name << " - " <<  typeString << " - Units: " <<  max_compute_units << " - Max work group size: " << max_work_group_size );
		
		commandQueue = clCreateCommandQueue( context, device, 0, &err );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to create command queue: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		// Note that SOURCE_DIR has beens specified outside, it is passed from CMake
		BOOST_TEST_MESSAGE( "Looking for " << "cluComplex.cl" << " in " << SOURCE_DIRECTORY );
		program = cluCreateProgramFromSource( context, "cluComplex.cl", SOURCE_DIRECTORY, &err );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to create program from source file: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		
		{
			std::stringstream tmp;
			
			tmp << "-I " << SOURCE_DIRECTORY;
			
#ifdef ADD_CL_INCLUDE_DIRS_COUNT
			const char* add_includes[] = ADD_CL_INCLUDE_DIRS;
			for( int i = 0; i < ADD_CL_INCLUDE_DIRS_COUNT; ++i )
				tmp << " -I " << add_includes[i];
#endif
			
			compiler_options = tmp.str().c_str();
		}
		BOOST_TEST_MESSAGE( "Build Options: " << compiler_options );
		
		// build program!
		
		err = clBuildProgram( program, 0, NULL, compiler_options, NULL, NULL);
		if( err )
		{
			cl_int new_err = clGetProgramBuildInfo( program, device, CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
			if( new_err )
			{
				BOOST_TEST_MESSAGE( "Failed to get build log: " << cluGetErrorString( new_err ) );
			}
			BOOST_TEST_MESSAGE( "Failed to build the program: " << cluGetErrorString( err ) << '\n' << log );
			throw std::exception();
		}
		
		
		err = clGetProgramBuildInfo( program, device, CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to get build log: " << cluGetErrorString( err ) );
			throw std::exception();
		}
		BOOST_TEST_MESSAGE( "Build Log: " << log );
	}

	~DeviceManagment()
	{
		cl_int err = clReleaseContext( context );
		if( err )
		{
			BOOST_TEST_MESSAGE( "Failed to release context: " << cluGetErrorString( err ) );
			throw std::exception();
		}
	}
};

struct RandomData32
{
	std::complex<float>* randomData32_1;
	std::complex<float>* randomData32_2;
	
	std::complex<float>* buffer32_1;	
	std::complex<float>* buffer32_2;	
	
	cl_mem randomData32_1_buffer;
	cl_mem randomData32_2_buffer;
	cl_mem buffer32_1_buffer;
	
	RandomData32()
	{
		randomData32_1 = new std::complex<float>[ TEST_SIZE ];
		randomData32_2 = new std::complex<float>[ TEST_SIZE ];

		buffer32_1 = new std::complex<float>[ TEST_SIZE ];
		buffer32_2 = new std::complex<float>[ TEST_SIZE ];
		for( size_t i = 0; i < TEST_SIZE; ++i )
		{
			randomData32_1[i] = std::complex<float>( 2.0f / static_cast<float>( rand() ), 2.0f / static_cast<float>( rand() ) );
			randomData32_2[i] = std::complex<float>( 2.0f / static_cast<float>( rand() ), 2.0f / static_cast<float>( rand() ) );
		}
		
		cl_int err;
		
		randomData32_1_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, TEST_SIZE * sizeof( std::complex<float> ), randomData32_1, &err );
		BOOST_REQUIRE_MESSAGE( ! err, "Failed to create buffer object: " << cluGetErrorString( err ) );
		
		randomData32_2_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, TEST_SIZE * sizeof( std::complex<float> ), randomData32_2, &err );
		BOOST_REQUIRE_MESSAGE( ! err, "Failed to create buffer object: " << cluGetErrorString( err ) );
		
		buffer32_1_buffer = clCreateBuffer( context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, &err );
		BOOST_REQUIRE_MESSAGE( ! err, "Failed to create buffer object: " << cluGetErrorString( err ) );
	}

	~RandomData32()
	{
		clReleaseMemObject( buffer32_1_buffer );
		clReleaseMemObject( randomData32_2_buffer );
		clReleaseMemObject( randomData32_1_buffer );
		
		delete[] randomData32_1;
		delete[] randomData32_2;

		delete[] buffer32_1;
		delete[] buffer32_2;
	}
};

struct RandomData64
{
	std::complex<double>* randomData64;

	RandomData64()
	{
		randomData64 = new std::complex<double>[ TEST_SIZE ];
		for( size_t i = 0; i < TEST_SIZE; ++i )
		{
			randomData64[i] = std::complex<double>( 2.0 / static_cast<double>( rand() ), 2.0 / static_cast<double>( rand() ) );
		}
	}

	~RandomData64()
	{
		delete[] randomData64;
	}
};

template<class T> void verify( std::complex<T>* left, std::complex<T>* right )
{
	for( size_t i = 0; i < TEST_SIZE; ++i )
	{
		BOOST_TEST_MESSAGE( "i = " << i );
		BOOST_REQUIRE_CLOSE( real( left[i] ), real( right[i] ), 0.1f );
		BOOST_REQUIRE_CLOSE( imag( left[i] ), imag( right[i] ), 0.1f );
	}
}

BOOST_GLOBAL_FIXTURE( DeviceManagment )

BOOST_FIXTURE_TEST_SUITE( onDevice, RandomData32 )

BOOST_AUTO_TEST_CASE( add32  )
{
	cl_int err;
	
	cl_kernel kernel = clCreateKernel( program, "add32", &err );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to create kernel: " << cluGetErrorString( err ) );
	
	err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &TEST_SIZE );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &randomData32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &randomData32_2_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	
	size_t workitems = TEST_SIZE;
	err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to enqueue kernel: " << cluGetErrorString( err ) );

	err = clEnqueueReadBuffer( commandQueue, buffer32_1_buffer, false, 0, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to read buffer: " << cluGetErrorString( err ) );

	err = clFlush( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to flush the command queue: " << cluGetErrorString( err ) );
	
	err = clFinish( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to wait for the command queue: " << cluGetErrorString( err ) );
	
	clReleaseKernel( kernel );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to release kernel: " << cluGetErrorString( err ) );

	// verify
	for( size_t i = 0; i < TEST_SIZE; ++i )
		buffer32_2[i] = randomData32_1[i] + randomData32_2[i];

	verify( buffer32_1, buffer32_2 );
}

BOOST_AUTO_TEST_CASE( sub32  )
{
	cl_int err;
	
	cl_kernel kernel = clCreateKernel( program, "sub32", &err );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to create kernel: " << cluGetErrorString( err ) );
	
	err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &TEST_SIZE );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &randomData32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &randomData32_2_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	
	size_t workitems = TEST_SIZE;
	err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to enqueue kernel: " << cluGetErrorString( err ) );
	
	err = clEnqueueReadBuffer( commandQueue, buffer32_1_buffer, false, 0, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to read buffer: " << cluGetErrorString( err ) );

	err = clFlush( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to flush the command queue: " << cluGetErrorString( err ) );
	
	err = clFinish( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to wait for the command queue: " << cluGetErrorString( err ) );
	
	clReleaseKernel( kernel );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to release kernel: " << cluGetErrorString( err ) );
	
	// verify
	for( size_t i = 0; i < TEST_SIZE; ++i )
		buffer32_2[i] = randomData32_1[i] - randomData32_2[i];
	
	verify( buffer32_1, buffer32_2 );
}

BOOST_AUTO_TEST_CASE( mul32  )
{
	cl_int err;
	
	cl_kernel kernel = clCreateKernel( program, "mul32", &err );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to create kernel: " << cluGetErrorString( err ) );
	
	err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &TEST_SIZE );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &randomData32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &randomData32_2_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	
	size_t workitems = TEST_SIZE;
	err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to enqueue kernel: " << cluGetErrorString( err ) );
	
	err = clEnqueueReadBuffer( commandQueue, buffer32_1_buffer, false, 0, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to read buffer: " << cluGetErrorString( err ) );

	err = clFlush( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to flush the command queue: " << cluGetErrorString( err ) );
	
	err = clFinish( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to wait for the command queue: " << cluGetErrorString( err ) );
	
	clReleaseKernel( kernel );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to release kernel: " << cluGetErrorString( err ) );
	
	// verify
	for( size_t i = 0; i < TEST_SIZE; ++i )
		buffer32_2[i] = randomData32_1[i] * randomData32_2[i];
	
	verify( buffer32_1, buffer32_2 );
}

BOOST_AUTO_TEST_CASE( div32  )
{
	cl_int err;
	
	cl_kernel kernel = clCreateKernel( program, "div32", &err );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to create kernel: " << cluGetErrorString( err ) );
	
	err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &TEST_SIZE );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &randomData32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &randomData32_2_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	
	size_t workitems = TEST_SIZE;
	err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to enqueue kernel: " << cluGetErrorString( err ) );
	
	err = clEnqueueReadBuffer( commandQueue, buffer32_1_buffer, false, 0, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to read buffer: " << cluGetErrorString( err ) );

	err = clFlush( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to flush the command queue: " << cluGetErrorString( err ) );
	
	err = clFinish( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to wait for the command queue: " << cluGetErrorString( err ) );
	
	clReleaseKernel( kernel );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to release kernel: " << cluGetErrorString( err ) );
	
	// verify
	for( size_t i = 0; i < TEST_SIZE; ++i )
		buffer32_2[i] = randomData32_1[i] / real( randomData32_2[i] );
	
	verify( buffer32_1, buffer32_2 );
}

BOOST_AUTO_TEST_CASE( div32_2  )
{
	cl_int err;
	
	cl_kernel kernel = clCreateKernel( program, "div32_2", &err );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to create kernel: " << cluGetErrorString( err ) );
	
	err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &TEST_SIZE );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &buffer32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &randomData32_1_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &randomData32_2_buffer );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to set argument: " << cluGetErrorString( err ) );
	
	size_t workitems = TEST_SIZE;
	err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to enqueue kernel: " << cluGetErrorString( err ) );
	
	err = clEnqueueReadBuffer( commandQueue, buffer32_1_buffer, false, 0, TEST_SIZE * sizeof( std::complex<float> ), buffer32_1, 0, NULL, NULL );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to read buffer: " << cluGetErrorString( err ) );

	err = clFlush( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to flush the command queue: " << cluGetErrorString( err ) );
	
	err = clFinish( commandQueue );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to wait for the command queue: " << cluGetErrorString( err ) );
	
	clReleaseKernel( kernel );
	BOOST_REQUIRE_MESSAGE( ! err, "Failed to release kernel: " << cluGetErrorString( err ) );
	
	// verify
	for( size_t i = 0; i < TEST_SIZE; ++i )
		buffer32_2[i] = randomData32_1[i] / randomData32_2[i];
	
	verify( buffer32_1, buffer32_2 );
}

BOOST_AUTO_TEST_SUITE_END()
