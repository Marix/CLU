/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include <clu.h>

#include <stdio.h>
#include <stdlib.h>

int main( __attribute__((unused)) int argc, __attribute__((unused)) char** argv )
{
	cl_int err;
	
	const cl_uint MAX_PLATFORMS = 4;
	cl_platform_id platforms[ MAX_PLATFORMS ];
	cl_uint nPlatforms = 0;
	
	cl_context context;
	cl_device_id devices[ 32 ];
	cl_uint nDevices;
	
	const cl_uint NUM_VALUES = 4096;
	float in_a[ NUM_VALUES ];
	float in_b[ NUM_VALUES ];
	float out[ NUM_VALUES ];
	
	cl_mem in_a_buffer, in_b_buffer, out_buffer;

	cl_uint i,j;
	
	for( i = 0; i < NUM_VALUES; ++i )
	{
		in_a[ i ] = 0.5f * i;
		in_b[ i ] = 0.5f * i;
	}
	
	// try to get the platform (currently it should always be one)
	err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
	if( err )
	{
		fprintf( stderr, "Failed to get platforms: %s", cluGetErrorString( err ) );
		return -1;
	}
	
	if( ! nPlatforms )
	{
		fprintf( stderr, "Failed to find an OpenCL platform: %s", cluGetErrorString( err ) );
		return -1;
	}
	
	if( nPlatforms > 1 )
	{
		printf( "Found more than one OpenCL platform, will use a random one." );
		return -1;
	}
	
	// currently there is always only one
	// As the AMD implementation already supports ICD we have to specify the platform
	// we want to use
	cl_context_properties context_props[3] = {
		CL_CONTEXT_PLATFORM,
		(cl_context_properties) platforms[0],
		0
	};
	
	context = clCreateContextFromType( context_props, CL_DEVICE_TYPE_ALL, NULL, NULL, &err );
	if( err )
	{
		fprintf( stderr, "Failed to create context: %s", cluGetErrorString( err ) );
		exit( -1 );
	}
	
	// get devices from context
	{
		size_t returned_size;
		err = clGetContextInfo( context, CL_CONTEXT_DEVICES, sizeof( devices ), devices, &returned_size);
		if( err )
		{
			fprintf( stderr, "Failed to create context: %s", cluGetErrorString( err ) );
			exit( -1 );
		}
		
		nDevices = returned_size / sizeof( cl_device_id );
	}
	
	// loop over the devices to get their properties
	for( j = 0; j < nDevices; ++j )
	{
		cl_device_type type;
		cl_uint vendor_id;
		cl_uint max_compute_units;
		size_t max_work_group_size;
		char name[ 64 ];
		size_t nName;
		cl_bool available;
		
		cl_command_queue commandQueue;
		cl_program program;
		cl_kernel kernel;
		
		char log[ 2048 ];
		size_t logSize;
		
		char* typeString;
		
		// TODO this should be dynamically sized (sizeof(SOURCE_DIRECTORY))
		char compiler_options[ 512 ];
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_TYPE, sizeof( type ), &type, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_TYPE: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_VENDOR_ID, sizeof( vendor_id ), &vendor_id, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_VENDOR_ID: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof( max_compute_units ), &max_compute_units, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_MAX_COMPUTE_UNITS: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof( max_work_group_size ), &max_work_group_size, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_MAX_WORK_GROUP_SIZE: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_NAME, sizeof( name ), name, &nName );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_NAME: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_AVAILABLE, sizeof( available ), &available, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_ENABLED: %s\n", cluGetErrorString( err ) );
			return -1;
		}
		
		if( ! available )
			continue;
		
		switch( type )
		{
			case CL_DEVICE_TYPE_CPU:
				typeString = "CPU";
				break;
			case CL_DEVICE_TYPE_GPU:
				typeString = "GPU";
				break;
			case CL_DEVICE_TYPE_ACCELERATOR:
				typeString = "ACCELERATOR";
				break;
			case CL_DEVICE_TYPE_DEFAULT:
				typeString = "DEFAULT";
				break;
			default:
				typeString = "UNKNOWN";
				break;
		}
		
		printf( "%s - %s - %ui x %li\n", name, typeString, max_compute_units, max_work_group_size );
		
		commandQueue = clCreateCommandQueue( context, devices[ j ], 0, &err );
		if( err )
		{
			fprintf( stderr, "Failed to create command queue: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		
		in_a_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof( in_a ), in_a, &err );
		if ( err ) {
			fprintf( stderr, "Failed to create buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		in_b_buffer = clCreateBuffer( context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof( in_b ), in_b, &err );
		if ( err ) {
			fprintf( stderr, "Failed to create buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		out_buffer = clCreateBuffer( context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof( out ), out, &err );
		if ( err ) {
			fprintf( stderr, "Failed to create buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		
		// no writing to buffer necessary!
		// Note that SOURCE_DIR has beens specified outside, it is passed from CMake
		program = cluCreateProgramFromSource( context, "kernel.cu", SOURCE_DIRECTORY, &err );
		if( err )
		{
			fprintf( stderr, "Failed to create program from source file: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		sprintf( compiler_options, "-I %s", SOURCE_DIRECTORY );
		printf( "Build Options: %s\n", compiler_options );
		err = clBuildProgram( program, 0, NULL, compiler_options, NULL, NULL);
		if( err )
		{
			fprintf( stderr, "Failed to build program: %s\n", cluGetErrorString( err ) );
			
			err = clGetProgramBuildInfo( program, devices[ j ], CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
			if( err )
			{
				fprintf( stderr, "Failed to get build log: %s\n", cluGetErrorString( err ) );
				exit(-1);
			}
			printf( "%s\n", log );
			
			exit(-1);
		}
		
		
		// build program!
		
		err = clGetProgramBuildInfo( program, devices[ j ], CL_PROGRAM_BUILD_LOG, sizeof( log ), log, &logSize );
		if( err )
		{
			fprintf( stderr, "Failed to get build log: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		printf( "%s\n", log );
		
		
		kernel = clCreateKernel( program, "add", &err );
		if( err ) 
		{
			fprintf( stderr, "Failed to create kernel: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		err = clSetKernelArg( kernel, 0, sizeof( cl_uint ), &NUM_VALUES );
		if( err )
		{
			fprintf( stderr, "Failed to set argument: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &out_buffer );
		if( err )
		{
			fprintf( stderr, "Failed to set argument: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &in_a_buffer );
		if( err )
		{
			fprintf( stderr, "Failed to set argument: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		err = clSetKernelArg( kernel, 3, sizeof( cl_mem ), &in_b_buffer );
		if( err )
		{
			fprintf( stderr, "Failed to set argument: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		size_t workitems = NUM_VALUES;
		err = clEnqueueNDRangeKernel( commandQueue, kernel, 1, NULL, &workitems, NULL, 0, NULL, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to enqueue kernel: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}

		err = clEnqueueReadBuffer( commandQueue, out_buffer, CL_FALSE, 0, sizeof( out ), out, 0, NULL, NULL );
		if( err )
		{
			fprintf( stderr, "Failed to read buffer: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}

		err = clFlush( commandQueue );
		if( err )
		{
			fprintf( stderr, "Failed to flush the command queue: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		err = clFinish( commandQueue );
		if( err )
		{
			fprintf( stderr, "Failed to wait for the command queue: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		clReleaseKernel( kernel );
		if( err )
		{
			fprintf( stderr, "Failed to release kernel: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		// no reading from buffer necessary!
		
		err = clReleaseMemObject( out_buffer );
		if ( err ) {
			fprintf( stderr, "Failed to release buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		err = clReleaseMemObject( in_b_buffer );
		if ( err ) {
			fprintf( stderr, "Failed to release buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		err = clReleaseMemObject( in_a_buffer );
		if ( err ) {
			fprintf( stderr, "Failed to release buffer object: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		err = clReleaseCommandQueue( commandQueue );
		if( err )
		{
			fprintf( stderr, "Failed to release command queue: %s\n", cluGetErrorString( err ) );
			exit(-1);
		}
		
		// check
		cl_bool correct = 1;
		for( i = 0; i < NUM_VALUES; ++i )
		{
			// this is not a correct float comparison
			if( out[ i ] != ( in_a[ i ] + in_b[ i ] ) )
			{
				correct = 0;
				break;
			}
		}
		if( correct )
			printf("Result correct\n");
		else
			printf("Result incorrect\n");
		
		
		if( j != nDevices - 1 )
			printf("\n");
		printf("\n");
	}
	
	err = clReleaseContext( context );
	if( err )
	{
		fprintf( stderr, "Failed to release context: %s\n", cluGetErrorString( err ) );
		exit( -1 );
	}
	
	return 0;
}


