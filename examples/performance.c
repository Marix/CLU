/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include <clu.h>

#include <stdio.h>
#include <stdlib.h>

int main( __attribute__((unused)) int argc, __attribute__((unused)) char** argv )
{
	cl_int err;

	const cl_uint MAX_PLATFORMS = 4;
	cl_platform_id platforms[ MAX_PLATFORMS ];
	cl_uint nPlatforms = 0;

	cl_context context;
	cl_device_id devices[ 32 ];
	cl_uint nDevices;

	cl_uint j;

	// try to get the platform (currently it should always be one)
	err = clGetPlatformIDs( MAX_PLATFORMS, platforms, &nPlatforms );
	if( err )
	{
		fprintf( stderr, "Failed to get platforms: %s", cluGetErrorString( err ) );
		return -1;
	}

	if( ! nPlatforms )
	{
		fprintf( stderr, "Failed to find an OpenCL platform: %s", cluGetErrorString( err ) );
		return -1;
	}

	if( nPlatforms > 1 )
	{
		printf( "Found more than one OpenCL platform, will use a random one." );
		return -1;
	}

	// currently there is always only one
	// As the AMD implementation already supports ICD we have to specify the platform
	// we want to use
	cl_context_properties context_props[3] = {
		CL_CONTEXT_PLATFORM,
		(cl_context_properties) platforms[0],
		0
	};

	context = clCreateContextFromType( context_props, CL_DEVICE_TYPE_ALL, NULL, NULL, &err );
	if( err )
	{
		fprintf( stderr, "Failed to create context: %s", cluGetErrorString( err ) );
		exit( -1 );
	}

	// get devices from context
	{
		size_t returned_size;
		err = clGetContextInfo( context, CL_CONTEXT_DEVICES, sizeof( devices ), devices, &returned_size);
		if( err )
		{
			fprintf( stderr, "Failed to create context: %s", cluGetErrorString( err ) );
			exit( -1 );
		}
		
		nDevices = returned_size / sizeof( cl_device_id );
	}

	// loop over the devices to query their performance
	for( j = 0; j < nDevices; ++j )
	{
		char name[ 64 ];
		size_t nName;
		float singlePrecision, doublePrecision, bandwidth;

		err = clGetDeviceInfo( devices[ j ], CL_DEVICE_NAME, sizeof( name ), name, &nName );
		if( err )
		{
			fprintf( stderr, "Failed to get device property - CL_DEVICE_NAME: %s\n", cluGetErrorString( err ) );
			return -1;
		}

		err = cluGetPeakPerformance( devices[ j ], CL_FALSE, &singlePrecision );
		if( err ) {
			fprintf( stderr, "Failed to get single precision performance of device: %s\n", cluGetErrorString( err ) );
			return -1;
		}

		err = cluGetPeakPerformance( devices[ j ], CL_TRUE, &doublePrecision );
		if( err ) {
			fprintf( stderr, "Failed to get double precision performance of device: %s\n", cluGetErrorString( err ) );
			return -1;
		}

		err = cluGetPeakBandwidth( devices[ j ], &bandwidth );
		if( err ) {
			fprintf( stderr, "Failed to get memory bandwidth of device: %s\n", cluGetErrorString( err ) );
			return -1;
		}

		printf( "%s - %f Gflops SP, %f Gflops DP, %f Gbyte/s Memory Bandwidth\n", name, singlePrecision, doublePrecision, bandwidth );
	}

	err = clReleaseContext( context );
	if( err )
	{
		fprintf( stderr, "Failed to release context: %s\n", cluGetErrorString( err ) );
		exit( -1 );
	}

	return 0;
}



