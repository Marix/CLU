/*
 * This file is part of CLU.
 * 
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include <clu.h>

#include <stdio.h>

int main( __attribute__((unused)) int argc, __attribute__((unused)) char** argv )
{
	printf( "CL_SUCCESS -> %s\n", cluGetErrorString( CL_SUCCESS ) );
	printf( "CL_OUT_OF_HOST_MEMORY -> %s\n", cluGetErrorString( CL_OUT_OF_HOST_MEMORY ) );

	return 0; 
}

