/*
 * This file is part of CLU.
 *
 * CLU is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CLU.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2010 Matthias Bach <marix@marix.org>
 */

#include "clu_rnd.h"

__kernel void randomize( unsigned int n_out, __global float* out, unsigned int n_taus, __global clu_taus_state* taus_state )
{
	int i_taus = get_global_id(0);
	if( i_taus < n_taus )
	{
		int i_out;
		for( i_out = i_taus; i_out < n_out; i_out += n_taus )
		{
			out[ i_out ] = clu_taus_rnd( &taus_state[ i_taus ] );
		}
	}
}

